const express = require('express');
const app = express();
const io = require('socket.io');
require('ejs');
app.set('view engine', 'ejs');
const bodyParser = require('body-parser');
app.use(
	bodyParser.json({
		limit: '5mb'
	})
);
app.use(
	bodyParser.urlencoded({
		extended: true,
		limit: '5mb'
	})
);
app.disable('x-powered-by');
app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
	res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
	if ('OPTIONS' === req.method) {
		//respond with 200
		res.sendStatus(200);
	} else {
		//move on
		next();
	}
});
app.use((err, req, res, next) => {
	console.error(err.stack);
	res.status(500).send("Sorry can't find that!");
});

const serialport = require('serialport');
const Readline = require('@serialport/parser-readline');
require('@serialport/bindings');
// serialport.Binding = MockBinding;
// MockBinding.createPort('ROBOT', { echo: true, record: true });
let COMPort;
let sp;
app.get('/', (req, res) => {
	res.render('home.ejs');
});
app.get('/list', (req, res) => {
	serialport
		.list()
		.then((list) => {
			res.send({
				success: true,
				lists: list.map((e) => {
					return e.path;
				})
			});
		})
		.catch(() => {
			res.send({ success: false, message: 'Error getting list.' });
		});
});
app.post('/changePort', (req, res) => {
	const { port } = req.body;
	if (port) {
		sp.close();
		COMPort = port;
		init(port);
		res.send('Changing to ' + port);
	} else {
		res.send('Port not defined');
	}
});
app.post('/stop', (req, res) => {
	sp.close();
	res.send('Stopping..');
});
app.post('/emitData', (req, res) => {
	sp.write(`BPM = ${Math.random().toFixed(2)}\n`);
	sp.write(`I RATIO = 1:1\n`);
	sp.write(`I TIME = ${Math.random().toFixed(2)} Sec\n`);
	sp.write(`E TIME = ${Math.random().toFixed(2)} Sec\n`);
	sp.write(`TIDAL = ${Math.random().toFixed(2)} Ml\n`);
	sp.write(`CPAP = ${Math.random().toFixed(2)} CM of H2O\n`);
	sp.write(`OXY SAT = ${Math.random().toFixed(2)} %\n`);
	setTimeout(() => {
		sp.write('SYSTEM OFF\n');
	}, 5000);
	res.send('Emitting');
});

let server = app.listen(3001, () => {
	console.log('Server online at port 3001.');
	init();
});
let socketServer = io(server);

socketServer.on('connection', (socket) => {
	socketServer.emit('info', { message: 'Connected to ' + COMPort });
});

function init(port = false) {
	if (port) {
		setNewPort('COM6');
	} else {
		serialport.list().then((list) => {
			if (list.length < 1) {
				return;
			} else {
				COMPort = list[0].path;
				setNewPort(COMPort);
			}
		});
	}
}
function setNewPort(port) {
	sp = new serialport(
		port,
		{
			baudRate: 19200,
			autoOpen: true
		},
		(err) => {
			console.log('Serial port error', err);
		}
	);
	const parser = new Readline('\n');
	sp.pipe(parser);
	sp.on('open', function() {
		socketServer.emit('info', { message: 'Connected to ' + COMPort });
		console.log('Serial port open on ' + COMPort);
	});
	parser.on('data', function(data) {
		data = data.replace('READY', '');
		socketServer.emit('data_recieved', { message: data });
	});
	sp.on('error', function(err) {
		console.log('Error: ', err.message);
	});
}
